const express = require('express')
const api = express.Router()
// const LOG = require('../utils/logger.js')
// const find = require('lodash.find')
// const remove = require('lodash.remove')

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------
api.get('/t14', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t14/index.ejs',
        { title: 'TeamName', layout: 'layout.ejs' })
})
api.get('/t14/a', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('about/t14/a/Vemula.ejs',
        { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
})
api.get('/t14/b', (req, res) => {
  console.log(`Handling GET ${req}`)
  return res.render('about/t14/b/index.ejs',
        { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
})
module.exports = api